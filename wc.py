#!/usr/bin/python3
from sys import argv, stdin
import fileinput
import re

def add(x, y):
    # """Поэлементное сложение списков"""
    return list(map(lambda a, b: a + b, x, y))

def key_process(keys, file_name, input_data):
    """Производим подсчеты в соответствии с ключами"""
    result = ''
    count_list = []
    for key in keys:
        if key == '-l':
            c = len(input_data)
            count_list.append(c)
            result += str(c) + '\t'
        elif key == '-w':
            c = sum([len(line.split()) for line in input_data])
            count_list.append(c)
            result += str(c) + '\t'
        elif key == '-c':
            c = sum([len(line) for line in input_data])
            count_list.append(c)
            result += str(c) + '\t'
    return count_list, print(result + file_name)

def read_files(argv, keys):
    """Считываем данные из файлов, пути которых указаны в argv"""
    data_all_files = {}
    if len(argv) > 1:
        last = False
        for ind, arg in enumerate(argv[1:]):
            if arg not in keys:
                last = False
                data_each_file = []
                try:
                    for line in fileinput.input(arg):
                        try:
                            data_each_file.append(line)
                        except(EOFError):
                            break
                except(FileNotFoundError):
                    print('{}: Данного файла или директории'
                          ' не существует'.format(arg))
                    last = True
                else:
                    data_all_files[arg] = data_each_file
    else:
        return data_all_files
    return data_all_files if (not last) or (data_all_files) else False

def read_lines(data_all_files):
    """Считываем данные из stdin"""
    if (not data_all_files) and (data_all_files != False):
        return stdin.readlines()
    else:
        return False

def check_for_keys(argv):
    """Составляем список ключей для текущего ввода"""
    keys = []
    for l in argv:
        if l.startswith('-') and (l in '-l-w-c'):
            keys.append(l)
        elif l == '-v':
            print("""Version 0.0.3. Данное программное обеспечение является
свободным: вы вправе изменять его и использовать в своих целях""")
            return False
        elif l == '-h':
            print("""Использование: wc [ключ]... [файл]
или: wc [файл]... [ключ]
Выводит информацию о количестве строк, слов и байт, а также итоговую информацию
о каждом файле, если передано более 1 файла. Словом считаются последовательности
символов ненулевой длины, разделенных пробелами символы.

В отсутствии переданного файла данные считываются из стандартного ввода(stdin).
Для окончания ввода данных необходимо нажать ctrl + D.

Ключи, указанные ниже могут, быть использованы для выбора, какие из подсчитанных
данных должны быть выведены: количество строк, количество слов или количество
байт. Если не указано не одного ключа, информация будет выведена в следующем
порядке: количество строк, количество слов, количество байт.
    -c      выводит количество подсчитанных байт
    -l      выводит количество подсчитанных строк
    -w      выводит количество подсчитанных слов
    -h      выводит данную информацию и завершает свою работу
    -v      выводит информацию о версии программы и завершает свою работу

Порядок вывода информации зависит от порядка следования ключей."""
            )
            return False
        elif '-' in l:
            print("Неизвестный ключ: {}\nВведите 'wc"
                  " -h' для вывода вспомогательной информации.".format(l))
            return False
    return set(keys)

def data_process(data_all_files, data):
    """Обрабатываем текст в соответствии с ключами
       в зависимости от самих ключей и типов данных (файлы или поток)"""
    if data_all_files:
        total = [0, 0, 0]
        for data_each_file in data_all_files:
            if keys:
                current, _ = key_process(keys,
                            data_each_file,
                            data_all_files[data_each_file]
                            )
                total = add(total, current)
            else:
                current, _ =  key_process(['-l', '-w', '-c'],
                            data_each_file,
                            data_all_files[data_each_file]
                            )
                total = add(total, current)
        if total != current:
            total_line = ''
            for i in total:
                if i != 0:
                    total_line += '{}\t'.format(i)
            print('{}total'.format(total_line))
    else:
        if data:
            if keys:
                key_process(keys, '', data)
            else:
                key_process(['-l', '-w', '-c'], '', data)


if __name__ == '__main__':

    keys = check_for_keys(argv)
    if keys != False:
        data_all_files = read_files(argv, keys)
        data = read_lines(data_all_files)
        data_process(data_all_files, data)
